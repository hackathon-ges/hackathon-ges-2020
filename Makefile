
install:
	docker-compose exec php bin/composer.phar install
start:
	docker-compose up -d
	docker-compose exec php bin/console server:run
